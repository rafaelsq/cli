package cli

import (
	"fmt"
	"testing"
)

func TestCommandLineToArgs(t *testing.T) {
	m, a := CommandLineToArgs("rm -rf this")
	if len(a) != 2 {
		t.Error(fmt.Errorf(" argument list doesnt match; %v", a))
	} else if a[0] != "rm" {
		t.Error(fmt.Errorf("argument must be \"rm\"; %v", a))
	} else if a[1] != "this" {
		t.Error(fmt.Errorf("argument must be \"this\"; %v", a))
	}

	if m["r"] != "this" {
		t.Error(fmt.Errorf("argument must be \"this\"; %v", m))
	}
	if m["f"] != "this" {
		t.Error(fmt.Errorf("argument must be \"this\"; %v", m))
	}

	m, a = CommandLineToArgs("rm -rf 'this thing' --all --nop \"do\" --end-life")
	if len(a) != 3 {
		t.Error(fmt.Errorf(" argument list doesnt match; %v", a))
	} else if a[0] != "rm" {
		t.Error(fmt.Errorf("argument must be \"rm\"; %v", a))
	} else if a[1] != "this thing" {
		t.Error(fmt.Errorf("argument must be \"this thing\"; %v", a))
	} else if a[2] != "do" {
		t.Error(fmt.Errorf("argument must be \"do\"; %v", a))
	}

	if m["r"] != "this thing" {
		t.Error(fmt.Errorf("argument must be \"this thing\"; %v", m))
	}
	if m["f"] != "this thing" {
		t.Error(fmt.Errorf("argument must be \"this thing\"; %v", m))
	}
	if m["all"] != "" {
		t.Error(fmt.Errorf("argument must be \"\"; %v", m))
	}
	if m["nop"] != "do" {
		t.Error(fmt.Errorf("argument must be \"do\"; %v", m))
	}
	if m["end-life"] != "" {
		t.Error(fmt.Errorf("argument must be \"\"; %v", m))
	}
}

func TestRightPad(t *testing.T) {
	str := rightPad("hi", "!", 5)
	if str != "hi!!!" {
		t.Error(fmt.Errorf("wrong right pad; %v", str))
	}
}

func TestLeftPad(t *testing.T) {
	str := leftPad("hi!", "hi ", 10)
	if str != "hi hi hi hi hi hi hi hi!" {
		t.Error(fmt.Errorf("wrong left pad; %v", str))
	}
}
