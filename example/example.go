package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/rafaelsq/cli"
)

func removeA(ctx context.Context) (context.Context, error) {
	fmt.Println("COMMAND:\n  Remove:")
	flags, args := cli.ParamsFromContext(ctx)
	if len(args) == 0 {
		return ctx, fmt.Errorf("must provide query")
	} else {
		var list []string
		raw := ctx.Value("list")
		if raw != nil {
			list = raw.([]string)
		}

		var rm []int
		for i, l := range list {
			if strings.Contains(l, args[0]) {
				fmt.Println("    ", l)
				rm = append(rm, i)
			}
		}

		if len(rm) == 1 {
			var newList []string
			for i, l := range list {
				if i != rm[0] {
					newList = append(newList, l)
				}
			}

			return context.WithValue(ctx, "list", newList), nil
		} else if _, has := flags["f"]; len(rm) > 1 && has {
			var newList []string
			for i, l := range list {
				if indexOf(rm, i) == -1 {
					newList = append(newList, l)
				}
			}

			return context.WithValue(ctx, "list", newList), nil
		} else if len(rm) > 1 {
			return ctx, fmt.Errorf("more than one row found, try with -f")
		} else {
			return ctx, fmt.Errorf("no row found")
		}
	}

	return ctx, nil
}

func indexOf(list []int, item int) int {
	for i, l := range list {
		if l == item {
			return i
		}
	}
	return -1
}

func listA(ctx context.Context) (context.Context, error) {
	fmt.Println("COMMAND:\n  List:")

	var list []string
	raw := ctx.Value("list")
	if raw != nil {
		list = raw.([]string)
	}

	_, args := cli.ParamsFromContext(ctx)
	if len(args) != 0 {
		for _, k := range list {
			if strings.Contains(k, args[0]) {
				fmt.Println("    ", k)
			}
		}
	} else {
		for _, k := range list {
			fmt.Println("    ", k)
		}
	}

	return ctx, nil
}

func newA(ctx context.Context) (context.Context, error) {
	fmt.Print("COMMAND:\n  New: ")
	_, args := cli.ParamsFromContext(ctx)
	if len(args) == 0 {
		fmt.Println("")
		return ctx, fmt.Errorf("must provide label")
	} else {
		var list []string
		raw := ctx.Value("list")
		if raw != nil {
			list = raw.([]string)
		}
		_, args := cli.ParamsFromContext(ctx)
		fmt.Println(args[0])
		ctx = context.WithValue(ctx, "list", append(list, args[0]))
	}

	return ctx, nil
}

func main() {
	cmds := []cli.CMD{
		{
			Name:   "remove",
			Alias:  "rm",
			Usage:  "remove a row",
			Args:   []string{"query", "-f"},
			Action: removeA,
		},
		{
			Name:   "list",
			Alias:  "ls",
			Usage:  "list or filter",
			Args:   []string{"query"},
			Action: listA,
		},
		{
			Name:   "new",
			Alias:  "n",
			Usage:  "create a new row",
			Args:   []string{"label"},
			Action: newA,
		},
	}

	cmds2 := append(cmds, cli.CMD{
		Name:  "cmd",
		Alias: "c",
		Usage: "sub terminal",
		Action: func(ctx context.Context) (context.Context, error) {
			buf := bufio.NewReader(os.Stdin)
			for {
				fmt.Print("> ")
				line, _, err := buf.ReadLine()
				if err == io.EOF {
					break

				} else if err != nil {
					fmt.Println("error: ", err)
					break
				}

				flags, args := cli.CommandLineToArgs(string(line))

				if len(args) != 0 {
					if cmd := cli.FindCommand(args[0], cmds); cmd != nil {
						ctx, err = cmd.Action(context.WithValue(context.WithValue(ctx, "cmd.flags", flags), "cmd.args", args[1:]))
						if err != nil {
							fmt.Println("    err:", err)
						}
						continue
					}
				}

				cli.PrintCommands(cmds)
			}
			return ctx, nil
		},
	})

	ci := cli.Cli{
		Name:    "CLI",
		CMDs:    cmds2,
		Context: context.Background(),
	}

	if err := ci.Run(os.Args[1:]); err != nil {
		panic(err)
	}
}
