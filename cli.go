package cli

import (
	"context"
	"fmt"
	"math"
	"strings"
)

type CMD struct {
	Name   string
	Usage  string
	Alias  string
	Args   []string
	Action func(context.Context) (context.Context, error)
}

func CommandLineToArgs(cmd string) (map[string]string, []string) {
	m := map[string]string{}
	a := []string{}
	keys := []string{}

	for _, raw := range parseCommandLine(cmd) {
		if strings.HasPrefix(raw, "--") {
			if len(keys) != 0 {
				for _, k := range keys {
					m[k] = ""
				}
			}

			keys = []string{string(raw[2:])}
		} else if strings.HasPrefix(raw, "-") {
			for _, c := range raw[1:] {
				keys = append(keys, string(c))
			}
		} else if len(keys) != 0 {
			for _, k := range keys {
				m[k] = raw
			}

			a = append(a, raw)
			keys = []string{}
		} else {
			a = append(a, raw)
		}
	}

	for _, k := range keys {
		m[k] = ""
	}

	return m, a
}

func parseCommandLine(cmd string) []string {
	var out []string
	var tmp *string
	var ch string
	var open rune
	for _, t := range cmd {
		if (t == '"' || t == '\'') && (open == t || tmp == nil) {
			if tmp == nil {
				e := ""
				tmp = &e
				open = t
			} else {
				if len(*tmp) != 0 {
					out = append(out, *tmp)
				}
				tmp = nil
			}
		} else if tmp == nil {
			if t == ' ' {
				if len(ch) != 0 {
					out = append(out, ch)
				}
				ch = ""
			} else {
				ch += string(t)
			}
		} else {
			e := *tmp + string(t)
			tmp = &e
		}
	}
	if len(ch) != 0 {
		out = append(out, ch)
	}

	return out
}

func PrintCommands(cmds []CMD) {
	fmt.Println("COMMANDS:")
	var col1 float64 = 0
	var col2 float64 = 0
	var lines [][]string
	for _, c := range cmds {
		alias := ""
		if len(c.Alias) != 0 {
			alias = ", " + c.Alias
		}
		col1 = math.Max(col1, float64(len(c.Name+alias)))
		col2 = math.Max(col2, float64(len(strings.Join(c.Args, ", "))+2))
		lines = append(lines, []string{
			c.Name + alias,
			"[" + strings.Join(c.Args, ", ") + "]",
			c.Usage,
		})
	}

	for _, l := range lines {
		fmt.Printf(" %s  %s  %s\n", rightPad(l[0], " ", int(col1)), rightPad(l[1], " ", int(col2)), l[2])
	}
}

func rightPad(str, pad string, lenght int) string {
	l := lenght - len(str)
	for i := 0; i < l; i++ {
		str = str + pad
	}
	return str
}

func leftPad(str, pad string, lenght int) string {
	l := lenght - len(str)
	for i := 0; i < l; i++ {
		str = pad + str
	}
	return str
}

func ParamsFromContext(ctx context.Context) (map[string]string, []string) {
	return ctx.Value("cmd.flags").(map[string]string), ctx.Value("cmd.args").([]string)
}

func FindCommand(cmd string, cmds []CMD) *CMD {
	for _, c := range cmds {
		if c.Name == cmd || c.Alias == cmd {
			return &c
		}
	}

	return nil
}

type Cli struct {
	Name       string
	Version    string
	CMDs       []CMD
	Context    context.Context
	BeforeEach *func(context.Context) context.Context
}

func (c *Cli) Print() {
	if c.Version == "" {
		c.Version = "0.0.0"
	}

	fmt.Printf("NAME:\n %s\n\nVERSION:\n %s\n\n", c.Name, c.Version)
	PrintCommands(c.CMDs)
}

func (c *Cli) Run(args []string) error {
	if len(args) > 0 {
		flags, args := CommandLineToArgs(strings.Join(args, " "))

		if cmd := FindCommand(args[0], c.CMDs); cmd != nil {
			ctx, err := cmd.Action(context.WithValue(context.WithValue(c.Context, "cmd.flags", flags), "cmd.args", args[1:]))
			if err != nil {
				PrintCommands([]CMD{*cmd})
				return err
			}
			c.Context = ctx
		} else {
			c.Print()
		}
	} else {
		c.Print()
	}

	return nil
}
